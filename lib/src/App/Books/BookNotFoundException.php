<?php declare(strict_types=1);

namespace App\Books;

class BookNotFoundException extends \Exception
{}