<?php declare(strict_types=1);

namespace App\Api;

use App\Http\HTTPClient;

abstract class BaseApi
{
    const BASE_API = 'https://symfony-skeleton.q-tests.com';
    
    private $_shouldAuthorize;

    private $_httpClient;

    public function __construct($shouldAuthorize = true)
    {
        $this->_shouldAuthorize = $shouldAuthorize;

        $this->_httpClient = new HTTPClient();
    }

    public function get($url, $headers = [])
    {
        return $this->_sendRequest(
            'GET', $url, [], $headers
        );
    }
    
    public function post($url, $postBody = [], $headers = [])
    {
        return $this->_sendRequest(
            'POST', $url, $postBody, $headers
        );
    }
    
    public function put($url, $putBody = [], $headers = [])
    {
        return $this->_sendRequest(
            'PUT', $url, $putBody, $headers
        );
    }
    
    public function delete($url, $headers = [])
    {
        return $this->_sendRequest(
            'DELETE', $url, [], $headers
        );
    }

    private function _sendRequest($method, $url, $body = [], $headers = [])
    {
        if (is_array($body) && !empty($body)) {
            $headers = array_merge($headers, [
                'Content-Type' => 'application/json'
            ]);
        }

        if ($this->_shouldAuthorize) {
            $headers = array_merge($headers, $this->_getAuthorizationHeaders());
        }

        return $this->_httpClient->sendRequest($method, $url, $body, $headers);
    }

    private function _getAuthorizationHeaders()
    {
        return [
            'Authorization' => 'Bearer '.$_SESSION['data']['token_key']
        ];
    }

    protected function _arrayToQueryString($array)
    {
        $pairs = [];

        foreach ($array as $item => $value) {
            $pairs[] = "$item=$value";
        }

        return '?'.implode("&", $pairs);
    }
}