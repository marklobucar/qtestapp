<?php declare(strict_types=1);

use \App\Authors\AuthorNotFoundException;
use \App\Di\Container;

/** @var \App\Authors\AuthorsService $authors_service */
$authors_service = Container::get('authorsService');

if (!isset($_SERVER['QUERY_STRING'])) {
    header('Location: /authors', true, 302);
    exit;
} else {
    $query = $_SERVER['QUERY_STRING'];
    $map = [];

    $parts = explode('&', $query);

    foreach ($parts as $part) {
        $params = explode('=', $part);

        $map[$params[0]] = $params[1];
    }

    $author_id = $map['author_id'];

    if ($authors_service->deleteAuthor($author_id)) {
        header('Location: /authors', true, 302);
        exit;
    }
}