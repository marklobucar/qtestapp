<?php declare(strict_types=1);

namespace App\Http;

use Exception;

class NotAuthorizedException extends Exception
{
}