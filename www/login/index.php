<?php

$error = $_GET['error'] ?? -1;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Log in</title>
</head>
<body>
    <h1>Log in</h1>
    <form action="login/login.php" method="POST">
        <label for="email">
            Email&nbsp;<br>
            <input type="email" name="email" id="email">
        </label>
        <br><br>
        <label for="password">
            Password&nbsp;<br>
            <input type="password" name="password" id="password">
        </label>
        <br><br>
        <input type="submit" value="Log in">
    </form>
    <?php
        echo match (intval($error))
        {
            -1 => '',
            401 => '<h4>Incorrect email or password.</h4>',
            500 => '<h4>Something went wrong. Please try again later.</h4>',
            default => '<h4>An unknown error occurred.</h4>'
        }
    ?>
</body>
</html>