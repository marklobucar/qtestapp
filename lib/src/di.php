<?php declare(strict_types=1);

use App\DI\Container;

Container::set('authService', new App\Auth\AuthService());
Container::set('authorsService', new App\Authors\AuthorsService());
Container::set('booksService', new App\Books\BooksService());