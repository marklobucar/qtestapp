<?php declare(strict_types=1);

use App\DI\Container;
use App\Util\ServerUtil;

/** @var \App\Authors\AuthorsService $authors_service */
$authors_service = Container::get('authorsService');

$options = $authors_service->getAvailableSortOptions();
$sorting = ServerUtil::queryStringToMap();

$authors = $authors_service->getAuthors($sorting);

function echoOptions($key, $availableSortOptions, $actualSortOptions)
{
    $options = [];
    foreach ($availableSortOptions[$key] as $option) {
        $str = '<option value="'.$option['value'].'" ';

        if (isset($actualSortOptions[$key]) && $actualSortOptions[$key] === $option['value']) {
            $str .= 'selected ';
        }

        $str .= '>'.$option['label'].'</option>';
        $options[] = $str;
    }

    echo implode('', $options);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List of authors</title>
    <style>
        table {
            border-collapse: collapse;
        }

        th, td {
            padding: 4px 6px;
        }

        table, th, td {
            border: 1px solid black;
        }

        .pages {
            border-top: 3px solid black;
            margin-top: 16px;
            width: 100%;
            display: flex;
            flex-flow: row nowrap;
            justify-content: space-evenly;

            font-size: 1.5em;
        }
    </style>
</head>
<body>
    <?php include('header.php'); ?>
    <h1>List of authors</h1>
    <hr>
    <form action="/authors" method="GET">
        <label for="query">
            Query&nbsp;
            <input type="text" name="query" id="query" placeholder="Search query" value="<?php echo $sorting['query'] ?? '' ?>">
        </label>
        &nbsp;&nbsp;
        <label for="orderBy">
            Order by&nbsp;
            <select name="orderBy" id="orderBy">
                <?php echoOptions('orderBy', $options, $sorting); ?>
            </select>
        </label>
        &nbsp;&nbsp;
        <label for="direction">
            Sort direction
            <select name="direction" id="direction">
                <?php echoOptions('direction', $options, $sorting); ?>
            </select>
        </label>
        &nbsp;&nbsp;
        <label for="limit">
            Page size&nbsp;
            <select name="limit" id="limit">
                <?php echoOptions('limit', $options, $sorting); ?>
            </select>
        </label>
        &nbsp;&nbsp;
        <input type="submit" value="Apply filter">
    </form>
    <hr>
    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>Date of birth</th>
                <th>Gender</th>
                <th>Place of birth</th>
                <th style="max-width: 256px;">Books</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($authors->getAuthors() as $author): ?>
                <tr>
                    <td>
                        <a href="<?php echo "author/get?author_id=".$author->getId(); ?>"><?php echo $author->getFullName(); ?></a>
                    </td>
                    <td>
                        <?php echo $author->getBirthdayFormatted(); ?>
                    </td>
                    <td>
                        <?php echo $author->getGender(); ?>
                    </td>
                    <td>
                        <?php echo $author->getPlaceOfBirth(); ?>
                    </td>
                    <td style="max-width: 256px;">
                        <?php
                            if (!empty($author->getBooks()))
                            {
                                $books = array_map(function ($book) { return "<a href=\"book/get?book_id={$book->getId()}\">{$book->getTitle()}</a>"; }, $author->getBooks());
                                echo implode(', ', $books);
                            }
                        ?>
                    </td>
                    <td>
                        <?php if (empty($author->getBooks())): ?>
                            <form action="author/delete" method="GET">
                                <input type="text" name="author_id" id="author_id" hidden tabindex="-1" value="<?php echo $author->getId(); ?>">
                                <input type="submit" value="Delete author">
                            </form>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="pages">
        <?php for ($page = 1; $page <= $authors->getPagesTotal(); $page++): ?>
            <?php if ($page === $authors->getCurrentPage()): ?>
                <span><?php echo $page; ?></span>
            <?php else: ?>
                <a href="<?php echo "/authors".ServerUtil::mapToQueryString(array_merge($sorting, ['page' => $page])) ?>"><?php echo $page; ?></a>
            <?php endif; ?>
        <?php endfor; ?>
    </div>
</body>
</html>