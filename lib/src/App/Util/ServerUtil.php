<?php declare(strict_types=1);

namespace App\Util;

class ServerUtil
{
    public static function queryStringToMap()
    {
        if (!isset($_SERVER['QUERY_STRING']))
        {
            return [];
        }

        $query = $_SERVER['QUERY_STRING'];
        $map = [];

        $parts = explode('&', $query);

        foreach ($parts as $part) {
            $params = explode('=', $part);

            $map[$params[0]] = $params[1];
        }

        return $map;
    }

    public static function mapToQueryString($map)
    {
        $str = '?';
        $pairs = [];

        foreach ($map as $name => $value)
        {
            $pairs[] = "$name=$value";
        }

        return $str.implode('&', $pairs);
    }
}