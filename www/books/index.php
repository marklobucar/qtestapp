<?php declare(strict_types=1);

use App\DI\Container;
use App\Util\ServerUtil;

/** @var \App\Books\BooksService $books_service */
$books_service = Container::get('booksService');

$options = $books_service->getAvailableSortOptions();
$sorting = ServerUtil::queryStringToMap();

$books = $books_service->getBooks($sorting);

function echoOptions($key, $availableSortOptions, $actualSortOptions)
{
    $options = [];
    foreach ($availableSortOptions[$key] as $option) {
        $str = '<option value="'.$option['value'].'" ';

        if (isset($actualSortOptions[$key]) && $actualSortOptions[$key] === $option['value']) {
            $str .= 'selected ';
        }

        $str .= '>'.$option['label'].'</option>';
        $options[] = $str;
    }

    echo implode('', $options);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Books</title>

    <style>
        table {
            border-collapse: collapse;
        }

        th, td {
            padding: 4px 6px;
        }

        table, th, td {
            border: 1px solid black;
        }

        .pages {
            border-top: 3px solid black;
            margin-top: 16px;
            width: 100%;
            display: flex;
            flex-flow: row nowrap;
            justify-content: space-evenly;

            font-size: 1.5em;
        }
    </style>
</head>
<body>
    <?php include('header.php'); ?>
    <h1>
        List of books
        &nbsp;&nbsp;&Barv;&nbsp;&nbsp;
        <a href="/book/new">Create new book</a>
    </h1>
    <hr>
    <form action="/books" method="GET">
        <label for="query">
            Query&nbsp;
            <input type="text" name="query" id="query" placeholder="Search query" value="<?php echo $sorting['query'] ?? '' ?>">
        </label>
        &nbsp;&nbsp;
        <label for="orderBy">
            Order by&nbsp;
            <select name="orderBy" id="orderBy">
                <?php echoOptions('orderBy', $options, $sorting); ?>
            </select>
        </label>
        &nbsp;&nbsp;
        <label for="direction">
            Sort direction
            <select name="direction" id="direction">
                <?php echoOptions('direction', $options, $sorting); ?>
            </select>
        </label>
        &nbsp;&nbsp;
        <label for="limit">
            Page size&nbsp;
            <select name="limit" id="limit">
                <?php echoOptions('limit', $options, $sorting); ?>
            </select>
        </label>
        &nbsp;&nbsp;
        <input type="submit" value="Apply filter">
    </form>
    <hr>
    <table>
        <thead>
            <tr>
                <th>Title</th>
                <th>ISBN</th>
                <th>Release date</th>
                <th>No. of pages</th>
                <th>Format</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($books->getBooks() as $book): ?>
                <tr>
                    <td>
                        <a href="<?php echo "book/get?book_id={$book->getId()}" ?>"><?php echo $book->getTitle(); ?></a>
                    </td>
                    <td>
                        <?php echo $book->getISBN(); ?>
                    </td>
                    <td>
                        <?php echo $book->getReleaseDateFormatted(); ?>
                    </td>
                    <td>
                        <?php echo $book->getNumberOfPages(); ?>
                    </td>
                    <td>
                        <?php echo $book->getFormat(); ?>
                    </td>
                    <td>
                        <form action="/book/delete" method="GET">
                            <input type="text" name="book_id" id="book_id" hidden tabindex="-1" value="<?php echo $book->getId(); ?>">
                            <input type="text" name="return_to" id="return_to" hidden tabindex="-1" value="/books">
                            <input type="submit" value="Delete book">
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="pages">
        <?php for ($page = 1; $page <= $books->getPagesTotal(); $page++): ?>
            <?php if ($page === $books->getCurrentPage()): ?>
                <span><?php echo $page; ?></span>
            <?php else: ?>
                <a href="<?php echo "/books".ServerUtil::mapToQueryString(array_merge($sorting, ['page' => $page])) ?>"><?php echo $page; ?></a>
            <?php endif; ?>
        <?php endfor; ?>
    </div>
</body>
</html>