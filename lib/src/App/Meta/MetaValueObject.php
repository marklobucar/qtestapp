<?php declare(strict_types=1);

namespace App\Meta;

abstract class MetaValueObject
{
    public function __construct($className, $initializationData)
    {
        $ref = new \ReflectionClass($className);

        foreach ($ref->getProperties() as $property)
        {
            $maybe_value = $initializationData[$this->_camelCaseToSnakeCase(ltrim($property->getName(), '_'))] ?? false;
            
            if ($maybe_value) {
                $property->setAccessible(true);
                $property->setValue($this, $maybe_value);
                $property->setAccessible(false);
            }
        }
    }

    // https://stackoverflow.com/a/35719689
    private function _camelCaseToSnakeCase($camelCasedString) {
        return strtolower(preg_replace(['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $camelCasedString));
    }

    private function _snakeCaseToCamelCase($snakeCasedString) { 
        return preg_replace('/(^|_)([a-z])/e', 'strtoupper("\\2")', $snakeCasedString); 
    }
}