<?php declare(strict_types=1);

namespace App\Authors;

use Exception;

class AuthorNotFoundException extends Exception
{}