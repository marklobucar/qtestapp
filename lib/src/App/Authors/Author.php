<?php declare(strict_types=1);

namespace App\Authors;

use App\Log\EchoLogger;
use App\Meta\MetaValueObject;

class Author extends MetaValueObject
{
    private $_id;

    private $_firstName;
    private $_lastName;

    private $_gender;

    private $_birthday;

    private $_placeOfBirth;

    /** @var \App\Books\Book[] */
    private $_books;

    public function __construct($data)
    {
        parent::__construct(get_class($this), $data);
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getFirstName()
    {
        return $this->_firstName;
    }

    public function getLastName()
    {
        return $this->_lastName;
    }

    public function getFullName()
    {
        return "{$this->_firstName} {$this->_lastName}";
    }

    public function getGender()
    {
        return $this->_gender;
    }

    public function getBirthday()
    {
        return $this->_birthday;
    }

    public function getBirthdayFormatted($format = 'm. d. Y')
    {
        return date($format, strtotime($this->_birthday));
    }

    public function getBirthdayTimestamp()
    {
        return strtotime($this->_birthday);
    }

    public function getPlaceOfBirth()
    {
        return $this->_placeOfBirth;
    }

    public function getBooks()
    {
        return match (empty($this->_books))
        {
            true => [],
            false => array_map(function ($book) { return new \App\Books\Book($book); }, $this->_books)
        };
    }

    public function __toString()
    {
        return get_class($this).' ['.$this->getId().']['.$this->getFullName().']['.$this->getBirthday().']['.$this->getPlaceOfBirth().']';
    }
}