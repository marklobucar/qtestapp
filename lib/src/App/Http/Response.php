<?php declare(strict_types=1);

namespace App\Http;

class Response
{
    private $_code;
    
    private $_data;

    private $_headers;

    public function __construct($code, $data, $headers = []) {
        $this->_code = $code;
        $this->_data = $data;
        $this->_headers = $headers;
    }

    public function getData($asString = false) {
        return $asString ? json_encode($this->_data) : $this->_data;
    }
    
    public function getHeaders() {
        return $this->_headers;
    }

    public function getHeader($header) {
        $header = strtolower($header);

        foreach ($this->_headers as $name => $value)
        {
            if ($header === strtolower($name)) {
                return $value;
            }
        }

        return false;
    }

    public function getStatusCode() {
        return $this->_code;
    }
}