<?php

declare(strict_types=1);

namespace App\Http;

use App\Http\Response;

class HTTPClient
{
    public function __construct()
    {
    }

    public function sendRequest($method, $url, $body = [], $headers = [])
    {
        $ch = curl_init();
        $response_headers = [];

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch,
            CURLOPT_HEADERFUNCTION,
            function ($curl, $header) use (&$response_headers) {
                $len = strlen($header);
                $header = explode(':', $header, 2);
                if (count($header) < 2) // ignore invalid headers
                    return $len;

                $response_headers[strtolower(trim($header[0]))][] = trim($header[1]);

                return $len;
            }
        );

        if (!empty($headers)) {
            $formatted_headers = [];

            foreach ($headers as $h => $v) {
                $formatted_headers[] = "$h: $v";
            }

            curl_setopt($ch, CURLOPT_HTTPHEADER, $formatted_headers);
        }

        if ($method === 'POST') {
            curl_setopt($ch, CURLOPT_POST,  true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        }

        if ($method === 'PUT') {
            curl_setopt($ch, CURLOPT_PUT, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        }

        if ($method === 'DELETE') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE"); 
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        }

        $response = curl_exec($ch);
        $info = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
        curl_close($ch);

        return new Response($info, json_decode($response, true), $response_headers);
    }
}
