<?php declare(strict_types=1);

function headedToAuth()
{
    return strpos($_SERVER['REQUEST_URI'], 'login') !== false || strpos($_SERVER['REQUEST_URI'], 'logout') !== false;
}

if (session_status() !== PHP_SESSION_ACTIVE)
{
    session_start();
}

if (empty($_SESSION) || !isset($_SESSION['data']))
{
    if (!headedToAuth()) { // Not headed to login or logout already
        header('Location: /login', true, 302);
        exit;
    }
}

if (isset($_SESSION['expires']) && time() > $_SESSION['expires'] && !headedToAuth())
{
    header('Location: /logout', true, 302);
    exit;
}