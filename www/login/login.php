<?php declare(strict_types=1);

use App\DI\Container;

$auth_service = Container::get('authService');

try {
    if (!isset($_POST['email']) || !isset($_POST['password'])) {
        echo 'Missing email or password.' . PHP_EOL;
        header('Location: /login?error=missing_data', true, 302);
        exit;
    }

    $email = $_POST['email'];
    $password = $_POST['password'];

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo "$email is a valid email address".PHP_EOL;
        header('Location: /login?error=invalid_email', true, 302);
        exit;
    }

    $auth_service->login($email, $password);

    header('Location: /authors', true, 302);
    exit;
} catch (\Exception $e) {
    header('Location: /login?error='.$e->getCode());
    exit;
}
