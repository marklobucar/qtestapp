<?php declare(strict_types=1);

namespace App\DI;

class Proxy
{
    private $_key;
    private $_actual;

    public function __construct($key)
    {
        $this->_key = $key;    
    }
    
    public function set($actual)
    {
        $this->_actual = $actual;
    }

    public function __call($method, $args)
    {
        if (!$this->_actual) {
            throw new \Exception('Component ['.$this->_key.'::'.$method.'] not set yet.');
        }

        $ret = call_user_func_array([$this->_actual, $method], $args);

        return $ret;
    }
}