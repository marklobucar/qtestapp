<?php declare(strict_types=1);

namespace App\Books;

use App\Api\BaseApi;

class BooksService extends BaseApi
{
    const DEFAULT_SORT_OPTIONS = [
        'query' => '',
        'orderBy' => 'id',
        'direction' => 'ASC',
        'limit' => 12,
        'page' => 1
    ];

    const AVAILABLE_SORT_OPTIONS = [
        'orderBy' => [
            ['value' => 'id', 'label' => 'ID'],
            ['value' => 'title', 'label' => 'Title'],
            ['value' => 'release_date', 'label' => 'Release date'],
            ['value' => 'isbn', 'label' => 'ISBN'],
            ['value' => 'format', 'label' => 'Format'],
            ['value' => 'number_of_pages', 'label' => 'Number of pages']
        ],
        'direction' => [
            ['value' => 'ASC', 'label' => 'Ascending'],
            ['value' => 'DESC', 'label' => 'Descending']
        ],
        'limit' => [
            ['value' => '12', 'label' => '12'],
            ['value' => '24', 'label' => '24'],
            ['value' => '36', 'label' => '36']
        ]
    ];

    const ENDPOINT = '/api/v2/books';

    public function __construct()
    {
        parent::__construct();
    }

    public function getBooks($sortOptions)
    {
        $sortOptions = array_merge(self::DEFAULT_SORT_OPTIONS, $sortOptions);

        $list = $this->get(parent::BASE_API.self::ENDPOINT.$this->_arrayToQueryString($sortOptions));

        if ($list->getStatusCode() !== 200) {
            throw new \Exception('An error occurred');
        }

        $books = $list->getData();

        $deserialized = [];

        foreach ($books['items'] as $item)
        {
            $deserialized[] = new Book($item);
        }

        return new BookList(
            $books['total_results'],
            $books['current_page'],
            $books['limit'],
            $books['total_pages'],
            $deserialized
        );
    }

    public function getBook($bookId)
    {
        $book = $this->get(parent::BASE_API.self::ENDPOINT."/$bookId");

        if ($book->getStatusCode() === 404) {
            throw new BookNotFoundException();
        }

        return new Book($book->getData());
    }

    public function createBook($authorId, $title, $releaseDate, $description, $isbn, $format, $numberOfPages)
    {
        if (!$authorId) {
            throw new \Exception('Missing author ID');
        }

        $post_data = [
            "author" => [
                "id" => intval($authorId)
            ],
            "title" => empty($title) ? 'Untitled book' : $title,
            "release_date" => empty($releaseDate) ? date(DATE_ATOM, time()) :$releaseDate,
            "description" => empty($description) ? 'N/A' : $description,
            "isbn" => empty($isbn) ? "9789200000005" : $isbn,
            "format" => empty($format) ? '6 x 9' : $format,
            "number_of_pages" => empty($numberOfPages) ? 0 : intval($numberOfPages)
        ];

        $res = $this->post(
            parent::BASE_API.self::ENDPOINT,
            $post_data
        );

        if ($res->getStatusCode() !== 200) {
            throw new \Exception('Could not create book');
        }

        return new Book($res->getData());
    }

    public function deleteBook($bookId)
    {
        if (!$bookId) {
            throw new \Exception('Missing book ID');
        }

        $res = $this->delete(
            parent::BASE_API.self::ENDPOINT."/$bookId"
        );

        if ($res->getStatusCode() !== 204) {
            throw new \Exception('Could not delete book');
        }

        return true;
    }

    public function getAvailableSortOptions()
    {
        return self::AVAILABLE_SORT_OPTIONS;
    }
}