<?php declare(strict_types=1);

use App\DI\Container;
use App\Util\ServerUtil;

/** @var App\Authors\AuthorsService $authors_service */
$authors_service = Container::get('authorsService');

$page = 1;
/** @var App\Authors\Author[] */
$available_authors = [];

do {
    $current_result = $authors_service->getAuthors(['page' => $page]);
    $available_authors = array_merge($available_authors, $current_result->getAuthors());
    $page++;
} while ($page <= $current_result->getPagesTotal());

$map = ServerUtil::queryStringToMap();
$error = isset($map['error']) ? urldecode($map['error']) : false;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create new book</title>
</head>
<body>
    <?php include('header.php'); ?>
    <a href="/books">&lt; Books</a>
    <h1>Create new book</h1>
    <hr>
    <form action="/book/new/create.php" method="POST">
        <label for="author_id">
            <b>Author</b>
            <br>
            <select name="author_id" id="author_id">
                <?php foreach ($available_authors as $author): ?>
                    <option value="<?php echo $author->getId(); ?>"><?php echo $author->getFullName(); ?></option>
                <?php endforeach; ?>
            </select>
        </label>
        <br><br>
        <label for="title">
            <b>Title</b>
            <br>
            <input type="text" name="title" id="title" placeholder="Book title">
        </label>
        <br><br>
        <label for="release_date">
            <b>Release date</b>
            <br>
            <input type="datetime-local" name="release_date" id="release_date">
        </label>
        <br><br>
        <label for="description">
            <b>Description</b>
            <br>
            <textarea name="description" id="description" cols="60" rows="10"></textarea>
        </label>
        <br><br>
        <label for="isbn">
            <b>ISBN</b>
            <br>
            <input type="number" name="isbn" id="isbn">
        </label>
        <br><br>
        <label for="format">
            <b>Format</b>
            <br>
            <input type="text" name="format" id="format">
        </label>
        <br><br>
        <label for="number_of_pages">
            <b>Number of pages</b>
            <br>
            <input type="number" name="number_of_pages" id="number_of_pages">
        </label>
        <br><br>
        <input type="submit" value="Create book">
    </form>
    <?php if ($error): ?>
        <hr>
        <h4 style="color: #692525;"><?php echo $error; ?></h4>
    <?php endif; ?>
</body>
</html>