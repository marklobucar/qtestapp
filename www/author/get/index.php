<?php

use App\Authors\AuthorNotFoundException;
use App\Authors\AuthorsService;
use App\Di\Container;
use App\Util\ServerUtil;

/** @var AuthorsService $authors_service */
$authors_service = Container::get('authorsService');

if (!isset($_SERVER['QUERY_STRING'])) {
    header('Location: /authors', true, 302);
    exit;
} else {
    $map = ServerUtil::queryStringToMap();

    $author_id = $map['author_id'];

    try {
        $author = $authors_service->getAuthor($author_id);
    } catch (AuthorNotFoundException $e) {
        header('Location: /authors', true, 302);
        exit;
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $author->getFullName(); ?></title>
    <style>
        table {
            border-collapse: collapse;
        }

        th, td {
            padding: 4px 6px;
        }

        table, th, td {
            border: 1px solid black;
        }
    </style>
</head>
<body>
    <?php include('header.php'); ?>
    <a href="/authors">&lt; Authors</a>
    <hr>
    <h3><?php echo $author->getFullName() ?></h3>
    <hr>
    <b>Gender: </b><?php echo $author->getGender() ?><br>
    <b>Date of birth: </b><?php echo $author->getBirthdayFormatted() ?><br>
    <b>Place of birth: </b><?php echo $author->getPlaceOfBirth() ?><br>
    <?php if (!empty($author->getBooks())): ?>
    <hr>
    <h4>Author's books</h4>
    <table>
        <thead>
            <tr>
                <th>Title</th>
                <th>ISBN</th>
                <th style="max-width: 400px">Description</th>
                <th>No. of pages</th>
                <th>Release date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($author->getBooks() as $book): ?>
                <tr>
                    <td><a href="<?php echo "/book/get?book_id={$book->getId()}"; ?>"><?php echo $book->getTitle(); ?></a></td>
                    <td><?php echo $book->getISBN(); ?></td>
                    <td style="max-width: 400px"><?php echo $book->getDescription(); ?></td>
                    <td><?php echo $book->getNumberOfPages(); ?></td>
                    <td><?php echo $book->getReleaseDateFormatted(); ?></td>
                    <td>
                        <form action="/book/delete" method="GET">
                            <input type="text" hidden tabindex="-1" name="return_to" id="return_to" value="<?php echo "/author/get?author_id={$author->getId()}" ?>">
                            <input type="text" hidden tabindex="-1" name="book_id" id="book_id_<?php echo $book->getId() ?>" value="<?php echo $book->getId() ?>">
                            <input type="submit" value="Delete book">
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</body>
</html>