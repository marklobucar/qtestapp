<?php declare(strict_types=1);

use App\DI\Container;

/** @var App\Books\BooksService $books_service */
$books_service = Container::get('booksService');

try {
    $new = $books_service->createBook(
        $_POST['author_id'],
        $_POST['title'],
        $_POST['release_date'],
        $_POST['description'],
        $_POST['isbn'],
        $_POST['format'],
        $_POST['number_of_pages']
    );

    header("Location: /book/get?book_id={$new->getId()}", true, 302);
    exit;
} catch (\Exception $e) {
    header("Location: /book/new?error=".urlencode($e->getMessage()), true, 302);
    exit;
}