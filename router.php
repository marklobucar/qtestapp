<?php declare(strict_types=1);

require_once('__include.php');
require_once('session_check.php');

// thanks to https://stackoverflow.com/a/38926070

$request_uri = $_SERVER['REQUEST_URI'];

if (strpos($request_uri, '?') !== false) {
    $pieces = explode('?', $request_uri);
    $request_uri = $pieces[0];
    $query = $pieces[1];
}

$requested_path = realpath(ltrim('www'.DIRECTORY_SEPARATOR.$request_uri, DIRECTORY_SEPARATOR));

if ($requested_path && is_dir($requested_path)) {
    $maybe_index = realpath($requested_path . DIRECTORY_SEPARATOR . 'index.php');
    if ($maybe_index) {
        $requested_path = $maybe_index;
    }
}

if ($requested_path && is_file($requested_path)) {
    if (strpos($requested_path, __DIR__ . DIRECTORY_SEPARATOR) === 0 &&
        $requested_path !== __DIR__ . DIRECTORY_SEPARATOR . 'router.php' &&
        substr(basename($requested_path), 0, 1) !== '.'
    ) {
        if (strtolower(substr($requested_path, -4)) === '.php') {
            include $requested_path;
        } else {
            return false;
        }
    } else {
        header("HTTP/1.1 404 Not Found");
        echo "404 Not Found";
    }
} else {
    header("HTTP/1.1 404 Not Found");
    echo "404 Not Found";
}