<?php

use App\Books\BookNotFoundException;
use App\Books\BooksService;
use App\Di\Container;

/** @var BooksService $books_service */
$books_service = Container::get('booksService');

if (!isset($_SERVER['QUERY_STRING'])) {
    header('Location: /books', true, 302);
    exit;
} else {
    $query = $_SERVER['QUERY_STRING'];
    $map = [];

    $parts = explode('&', $query);

    foreach ($parts as $part) {
        $params = explode('=', $part);

        $map[$params[0]] = $params[1];
    }

    $book_id = $map['book_id'];

    try {
        $book = $books_service->getBook($book_id);
    } catch (BookNotFoundException $e) {
        header('Location: /books', true, 302);
        exit;
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $book->getTitle(); ?></title>
</head>
<body>
    <?php include('header.php'); ?>
    <a href="/books">&lt; Books</a>
    <hr>
    <h3><?php echo $book->getTitle() ?></h3>
    <hr>
    <b>ISBN: </b><?php echo $book->getISBN() ?><br>
    <b>Release date: </b><?php echo $book->getReleaseDateFormatted() ?><br>
    <b>Format: </b><?php echo $book->getFormat() ?><br>
    <b>Description: </b><p style="max-width: 300px;"><?php echo $book->getDescription(); ?></p>
    <hr>
    <form action="/book/delete" method="GET">
        <input type="text" name="book_id" id="book_id" hidden tabindex="-1" value="<?php echo $book->getId(); ?>">
        <input type="text" name="return_to" id="return_to" hidden tabindex="-1" value="/books">
        <input type="submit" value="Delete book">
    </form>
</body>
</html>