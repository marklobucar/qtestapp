<?php declare(strict_types=1);

namespace App\Auth;

use App\Api\BaseApi;
use App\Http\NotAuthorizedException;

class AuthService extends BaseApi
{
    const ENDPOINT = '/api/v2/token';

    const EXPIRATION_ONE_DAY = 60 * 60 * 24;

    public function __construct()
    {
        parent::__construct(false);
    }

    public function login($email, $password)
    {
        $response = $this->post(parent::BASE_API.self::ENDPOINT, [
            'email' => $email, 'password' => $password
        ]);

        if ($response->getStatusCode() === 401) {
            throw new NotAuthorizedException('Incorrect email or password.', 401);
        }

        if ($response->getStatusCode() >= 500) {
            throw new \Exception('Something went wrong. Please try again later.', 500);
        }

        $data = $response->getData();

        if (!isset($data['token_key'])) {
            throw new \Exception('Something went wrong. Please try again later.', 500);
        }

        session_regenerate_id(true);

        $_SESSION['data'] = $data;
        $_SESSION['expires'] = time() + self::EXPIRATION_ONE_DAY;
    }

    public function logout()
    {
        if (session_status() === PHP_SESSION_ACTIVE)
        {
            session_start();
        }

        $_SESSION = [];

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        session_destroy();

        header('Location: /login', true, 302);
        exit();
    }
}