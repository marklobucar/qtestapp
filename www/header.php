<?php declare(strict_types=1);

$user = $_SESSION['data']['user'];

?>
<header class="header" style="border-bottom: 3px solid black; padding: 1em 1.5em; margin-bottom: 1em;">
    <span>Hello <?php echo $user['first_name'] . ' ' . $user['last_name']; ?></span>
    &nbsp;&nbsp;&Barv;&nbsp;&nbsp;
    <a href="/authors">Authors</a>
    &nbsp;&#124;&nbsp;
    <a href="/books">Books</a>
    <a style="float: right;" href="/logout">Log out</a>
</header>