<?php declare(strict_types=1);

use App\DI\Container;

$auth_service = Container::get('authService');
$auth_service->logout();