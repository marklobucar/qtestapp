<?php declare(strict_types=1);

use App\Books\BooksService;
use App\DI\Container;

/** @var BooksService $books_service */
$books_service = Container::get('booksService');

if (!isset($_SERVER['QUERY_STRING'])) {
    header('Location: /books', true, 302);
    exit;
} else {
    $book_id = $_GET['book_id'] ?? null;
    $return_to = $_GET['return_to'] ?? '/books';

    if ($book_id === null) {
        header('Location: '.$return_to, true, 302);
        exit;
    }

    try {
        $books_service->deleteBook($book_id);
        header('Location: '.$return_to, true, 302);
        exit;
    } catch (\Exception $e) {
        header('Location: '.$return_to, true, 302);
        exit;
    }
}