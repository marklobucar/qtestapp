<?php declare(strict_types=1);

namespace App\Books;

class BookList
{
    private $_resultsTotal;
    private $_currentPage;
    private $_pageSize;
    private $_pagesTotal;
    
    /**
     * @var Book[]
     */
    private $_books;

    public function __construct($resultsTotal, $currentPage, $pageSize, $pagesTotal, $books)
    {
        $this->_resultsTotal = $resultsTotal;
        $this->_currentPage = $currentPage;
        $this->_pageSize = $pageSize;
        $this->_pagesTotal = $pagesTotal;

        $this->_books = $books;
    }

    public function getResultsTotal()
    {
        return $this->_resultsTotal;
    }

    public function getCurrentPage()
    {
        return $this->_currentPage;
    }

    public function getPageSize()
    {
        return $this->_pageSize;
    }

    public function getPagesTotal()
    {
        return $this->_pagesTotal;
    }

    /**
     * @return Book[]
     */
    public function getBooks(): array
    {
        return $this->_books;
    }
}