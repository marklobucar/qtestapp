### Requirements
Requires PHP 8 and onwards.

### Running
Navigate to the directory root (here) and run `php -S localhost:8080 router.php`