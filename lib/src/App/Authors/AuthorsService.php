<?php declare(strict_types=1);

namespace App\Authors;

use App\Api\BaseApi;
use App\Authors\AuthorNotFoundException;
use App\Log\EchoLogger;
use Exception;

class AuthorsService extends BaseApi
{
    const DEFAULT_SORT_OPTIONS = [
        'query' => '',
        'orderBy' => 'id',
        'direction' => 'ASC',
        'limit' => 12,
        'page' => 1
    ];

    const AVAILABLE_SORT_OPTIONS = [
        'orderBy' => [
            ['value' => 'id', 'label' => 'ID'],
            ['value' => 'first_name', 'label' => 'First name'],
            ['value' => 'last_name', 'label' => 'Last name'],
            ['value' => 'birthday', 'label' => 'Birthday'],
            ['value' => 'gender', 'label' => 'Gender'],
            ['value' => 'place_of_birth', 'label' => 'Place of birth']
        ],
        'direction' => [
            ['value' => 'ASC', 'label' => 'Ascending'],
            ['value' => 'DESC', 'label' => 'Descending']
        ],
        'limit' => [
            ['value' => '12', 'label' => '12'],
            ['value' => '24', 'label' => '24'],
            ['value' => '36', 'label' => '36']
        ]
    ];

    const ENDPOINT = '/api/v2/authors';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Gets a list of authors
     * @param array $sortOptions Sort options for the list
     * @return AuthorList A list of all fetched authors
     * @throws Exception 
     */
    public function getAuthors($sortOptions = [])
    {
        $sortOptions = array_merge(self::DEFAULT_SORT_OPTIONS, $sortOptions);

        $list = $this->get(parent::BASE_API.self::ENDPOINT.$this->_arrayToQueryString($sortOptions));

        if ($list->getStatusCode() !== 200) {
            throw new \Exception('An error occurred');
        }

        $authors = $list->getData();
        $deserialized = [];

        foreach ($authors['items'] as $author)
        {
            try {
                $deserialized[] = $this->getAuthor($author['id']);
            } catch (AuthorNotFoundException $e) {
                continue;
            }
        }

        return new AuthorList(
            $authors['total_results'],
            $authors['current_page'],
            $authors['limit'],
            $authors['total_pages'],
            $deserialized
        );
    }

    public function getAuthor($authorId)
    {
        $author = $this->get(parent::BASE_API.self::ENDPOINT."/$authorId");

        if ($author->getStatusCode() === 404) {
            throw new AuthorNotFoundException();
        }

        return new Author($author->getData());
    }

    public function deleteAuthor($authorId)
    {
        $res = $this->delete(parent::BASE_API.self::ENDPOINT."/$authorId");
        EchoLogger::log('Got res ['.$res->getStatusCode().']['.print_r($res->getData(), true).']');

        if ($res->getStatusCode() !== 204) {
            throw new \Exception("Could not delete author [$authorId]");
        }

        return true;
    }

    public function getAvailableSortOptions()
    {
        return self::AVAILABLE_SORT_OPTIONS;
    }
}