<?php declare(strict_types=1);

use App\DI\Container;

set_include_path(get_include_path().PATH_SEPARATOR.realpath(__DIR__.DIRECTORY_SEPARATOR.'www'));

require_once(realpath(__DIR__.'/lib/src/App/DI/Container.php'));

Container::init(__DIR__.'/lib/src');
require_once(realpath(__DIR__.'/lib/src/di.php')); // DI requiring