<?php declare(strict_types=1);

namespace App\Books;

use App\Meta\MetaValueObject;

class Book extends MetaValueObject
{
    private $_id;

    private $_isbn;

    private $_title;
    private $_description;

    private $_releaseDate;
    private $_format;
    private $_numberOfPages;

    public function __construct($data)
    {
        parent::__construct(get_class($this), $data);
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getISBN()
    {
        return $this->_isbn;
    }

    public function getTitle()
    {
        return $this->_title;
    }

    public function getDescription()
    {
        return $this->_description;
    }

    public function getReleaseDateFormatted($format = 'Y-m-d')
    {
        return date($format, strtotime($this->_releaseDate));
    }

    public function getFormat()
    {
        return $this->_format;
    }

    public function getNumberOfPages()
    {
        return $this->_numberOfPages;
    }
}