<?php declare(strict_types=1);

namespace App\Authors;

class AuthorList
{
    private $_resultsTotal;
    private $_currentPage;
    private $_pageSize;
    private $_pagesTotal;
    
    /**
     * @var Author[]
     */
    private $_authors;

    public function __construct($resultsTotal, $currentPage, $pageSize, $pagesTotal, $authors)
    {
        $this->_resultsTotal = $resultsTotal;
        $this->_currentPage = $currentPage;
        $this->_pageSize = $pageSize;
        $this->_pagesTotal = $pagesTotal;

        $this->_authors = $authors;
    }

    public function getResultsTotal()
    {
        return $this->_resultsTotal;
    }

    public function getCurrentPage()
    {
        return $this->_currentPage;
    }

    public function getPageSize()
    {
        return $this->_pageSize;
    }

    public function getPagesTotal()
    {
        return $this->_pagesTotal;
    }

    /**
     * @return Author[]
     */
    public function getAuthors(): array
    {
        return $this->_authors;
    }


}